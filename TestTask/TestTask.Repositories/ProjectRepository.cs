﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestTask.Models;
using TestTask.Storage;

namespace TestTask.Repositories
{
    public class ProjectRepository : Repository<ProjectModel>
    {
        public ProjectRepository(ApplicationContext context) : base(context) { }

        public async Task<List<ProjectModel>> GetProjectsByUser(int userId, int skip, int take, string title, string company, string type)
        {
            return await Task.Run(() =>
            {
                var res = Request(title, company, type, userId).Skip(skip).Take(take).ToListAsync();
                return res;
            }).ConfigureAwait(false);
        }

        public async Task<ProjectModel> GetProjectById(int id)
        {
            return await Task.Run(() =>
            {
                var res = Entities.Where(m => m.Id == id).Include(s => s.User).Include(s => s.Type).Include(s => s.SkillProject).ThenInclude(m => m.Skill).FirstOrDefault();
                return res;
            }).ConfigureAwait(false);
        }

        public async Task<List<ProjectModel>> GetAllProjects(int skip, int take, string title, string company, string type)
        {
            return await Task.Run(() =>
            {
                return Request(title, company, type).Skip(skip).Take(take).ToListAsync();
            }).ConfigureAwait(false);
        }

        public async Task<int> GetProjectsCount(int userId, string title, string company, string type)
        {
            return await Task.Run(() =>
            {
                return Request(title, company, type, userId).Count();
            }).ConfigureAwait(false);
        }

        private IQueryable<ProjectModel> Request(string title, string company, string type, int userId = 0)
        {
            IQueryable<ProjectModel> request = Entities.AsQueryable();

            if (userId != 0)
                request = request.Where(s => s.User.Id == userId).Include(s => s.User).Include(s => s.SkillProject).ThenInclude(m => m.Skill).Include(s => s.Type);
            if(!string.IsNullOrEmpty(title))
                request = request.Where(s => s.Title == title).Include(s => s.User).Include(s => s.SkillProject).ThenInclude(m => m.Skill).Include(s => s.Type);            
            if(!string.IsNullOrEmpty(company))
                request = request.Where(s => s.Organization == company).Include(s => s.User).Include(s => s.SkillProject).ThenInclude(m => m.Skill).Include(s => s.Type);
            if(!string.IsNullOrEmpty(type))
                request = request.Where(s => s.Type.Name == type).Include(s => s.User).Include(s => s.SkillProject).ThenInclude(m => m.Skill).Include(s => s.Type);

            return request.Include(s => s.User).Include(s => s.Type).Include(s => s.SkillProject).ThenInclude(m => m.Skill);
        }

    }

}
