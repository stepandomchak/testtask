﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestTask.Models;
using TestTask.Storage;

namespace TestTask.Repositories
{
    public class UserRepository : Repository<UserModel>
    {
        public UserRepository(ApplicationContext context) : base(context) { }

        public async Task<UserModel> GetUserByLoginAndPassword(string login, string password)
        {
            return await Task.Run(() =>
            {
                return Entities.Where(m => m.Login == login && m.Password == password).FirstOrDefault();
            }).ConfigureAwait(false);
        }

        public async Task<UserModel> GetUserById(int id)
        {
            return await Task.Run(() =>
            {
                return Entities.Where(m => m.Id == id).Include(m=>m.Projects).FirstOrDefault();
            }).ConfigureAwait(false);
        }

        public async Task<List<UserModel>> GetAllUsers()
        {
            return await Task.Run(() =>
            {
                return this.Entities.Include(s => s.Projects).ToListAsync();
            }).ConfigureAwait(false);
        }

    }
}
