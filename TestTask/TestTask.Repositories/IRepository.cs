﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using TestTask.Models;

namespace TestTask.Repositories
{
    interface IRepository<T> where T : BaseModel
    {
        Task<T> Get(int id);

        Task<T> Create(T item);

        Task<T> Edit(T item);

        Task<T> Delete(T item);

        Task<IQueryable<T>> Where(Expression<Func<T, bool>> predicate);
    }
}
