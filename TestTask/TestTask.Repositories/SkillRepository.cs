﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestTask.Models;
using TestTask.Storage;

namespace TestTask.Repositories
{
    public class SkillRepository : Repository<SkillModel>
    {
        public SkillRepository(ApplicationContext context) : base(context) { }

        public async Task<SkillModel> GetSkillByName(string name)
        {
            return await Task.Run(() =>
            {
                return Entities.Where(m => m.Skill == name).FirstOrDefault();
            }).ConfigureAwait(false);

        }

    }
}
