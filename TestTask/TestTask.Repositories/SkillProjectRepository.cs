﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestTask.Models;
using TestTask.Storage;

namespace TestTask.Repositories
{
    public class SkillProjectRepository: Repository<SkillProjectModel>
    {
        public SkillProjectRepository(ApplicationContext context) : base(context) { }

        //public async Task<SkillProjectModel> DeleteSkillProject(SkillProjectModel model)
        //{
        //    return await Task.Run(() =>
        //    {
        //        Entities.Remove(model);
        //        return model;
        //    }).ConfigureAwait(false);

        //}

        public async Task<SkillProjectModel> GetSkillProject(int projectId, int skillId)
        {
            return await Task.Run(() =>
            {
                return Entities.Where(s => s.ProjectId == projectId && s.SkillId == skillId).FirstOrDefault();
            }).ConfigureAwait(false);

        }


    }
}
