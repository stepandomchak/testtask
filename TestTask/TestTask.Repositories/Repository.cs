﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using TestTask.Models;
using TestTask.Storage;

namespace TestTask.Repositories
{
    public class Repository<T> : IDisposable, IRepository<T> where T : BaseModel
    {
        private readonly ApplicationContext context;
        private DbSet<T> dbSet;
        private bool disposed = false;

        public Repository(ApplicationContext context)
        {
            this.context = context;
        }

        protected DbSet<T> Entities
        {
            get
            {
                if (dbSet == null)
                    dbSet = context.Set<T>();
                return dbSet;
            }
        }

        public async Task<T> Create(T item)
        {
            this.Entities.Add(item);
            await context.SaveChangesAsync().ConfigureAwait(true);
            return item;

        }

        public async Task<T> Delete(T item)
        {
            context.Entry(item).State = EntityState.Deleted;
            await context.SaveChangesAsync().ConfigureAwait(false);
            return item;
        }

        public async Task<T> Get(int id)
        {
            return await Task.Run(() =>
            {
                return this.Entities.Where(s => s.Id == id).FirstOrDefault();
            }).ConfigureAwait(false);
        }

        public async Task<T> Edit(T item)
        {
            if (!this.Entities.Local.Any(e => e == item))
                this.Entities.Attach(item);

            await context.SaveChangesAsync().ConfigureAwait(false);

            return item;
        }

        public async Task<IQueryable<T>> Where(Expression<Func<T, bool>> predicate)
        {
            return await Task.Run(() =>
            {
                var res = this.Entities.Where(predicate);
                return res;
            }).ConfigureAwait(false);
        }


        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
