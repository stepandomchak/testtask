﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestTask.Models;
using TestTask.Storage;

namespace TestTask.Repositories
{
    public class ProjectTypeRepository : Repository<ProjectTypeModel>
    {
        public ProjectTypeRepository(ApplicationContext context) : base(context) { }

        public async Task<List<ProjectTypeModel>> GetProjectTypes()
        {
            return await Task.Run(() =>
            {
                return Entities.ToList();
            }).ConfigureAwait(false);
        }

        public async Task<ProjectTypeModel> GetTypeByName(string typeName)
        {
            return await Task.Run(() =>
            {
                return Entities.Where(m => m.Name == typeName).FirstOrDefault();
            }).ConfigureAwait(false);
        }

    }
}
