﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Security.Cryptography;
using System.Text;
using TestTask.Models;

namespace TestTask.Storage
{
    public class ApplicationContext : DbContext
    {

        public DbSet<UserModel> Users { get; set; }
        public DbSet<ProjectModel> Projects { get; set; }

        public DbSet<SkillModel> Skills{ get; set; }
        public DbSet<ProjectTypeModel> Types{ get; set; }
        
        
        public ApplicationContext(DbContextOptions<ApplicationContext> options) : base(options)
        {
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer("Server=DESKTOP-B59MBLI\\MSSQLSERVER01;Database=TestTaskDb;Trusted_Connection=True;MultipleActiveResultSets=true");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {

            modelBuilder.Entity<UserModel>()
                .HasIndex(s => s.Login)
                .IsUnique(true);

            modelBuilder.Entity<ProjectModel>()
                .Property(s => s.Id)
                .ValueGeneratedOnAdd();

            modelBuilder.Entity<SkillModel>()
                .HasIndex(s => s.Skill)
                .IsUnique(true);

            modelBuilder.Entity<SkillProjectModel>()
                .HasOne(sc => sc.Project)
                .WithMany(s => s.SkillProject)
                .HasForeignKey(sc => sc.ProjectId);

            modelBuilder.Entity<SkillProjectModel>()
                .HasOne(sc => sc.Skill)
                .WithMany(s => s.SkillProject)
                .HasForeignKey(sc => sc.SkillId);

        }

    }
}
