﻿"use strict";

var gulp = require("gulp"),
    sass = require("gulp-sass"); 

var paths = {
    webroot: "./wwwroot/"
};

gulp.task("sass", function () {
    return gulp.src('Sass/style.scss')
        .pipe(sass())
        .pipe(gulp.dest(paths.webroot + '/css'));
});