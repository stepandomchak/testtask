﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Mvc;
using TestTask.Models;
using TestTask.Services.Interfaces;

namespace TestTask.Controllers
{
    public class AccountController : Controller
    {
        private readonly IUserService _userService;

        public IActionResult Index()
        {
            return View();
        }

        public AccountController(IUserService userService)
        {
            _userService = userService;
        }

        [HttpGet]
        public IActionResult Login()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Login(LoginModel model)
        {
            if (ModelState.IsValid)
            {
                UserModel user = await _userService.GetUserByEmailAndPassword(model.Login, GetHash(model.Password));
                if (user != null)
                {
                    await Authenticate(user.Id, user.Login, (int)user.Role);
                    return Redirect("~/Project/Index");
                }
            }
            return View(model);
        }


        [HttpGet]
        public IActionResult Register()
        {
            if(HttpContext.User.Identity.Name != null)
                return Redirect("~/Project/Index");

            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Register(RegistrationModel model)
        {
            if (ModelState.IsValid)
            {
                UserModel user = await _userService.CreateUser(new UserModel() { Login = model.Login, Password = GetHash(model.Password), Role = model.Role});
                if (user != null)
                {
                    await Authenticate(user.Id, user.Login, (int)user.Role);
                    return Redirect("~/Project/Index");
                }
            }
            return View(model);
        }


        private async Task Authenticate(int userId, string login, int role)
        {
            var claims = new List<Claim>
            {
                new Claim(ClaimsIdentity.DefaultNameClaimType, login),
                new Claim(ClaimsIdentity.DefaultNameClaimType, Convert.ToString(userId)),
                new Claim(ClaimsIdentity.DefaultRoleClaimType, Convert.ToString(role))
            };
            ClaimsIdentity id = new ClaimsIdentity(claims, "ApplicationCookie", ClaimsIdentity.DefaultNameClaimType, ClaimsIdentity.DefaultRoleClaimType);
            await HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, new ClaimsPrincipal(id));
        }

        public async Task<IActionResult> Logout()
        {
            await HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
            return RedirectToAction("Index", "Home");
        }

        
        public string GetHash(string s)
        {
            byte[] bytes = Encoding.Unicode.GetBytes(s);

            MD5CryptoServiceProvider CSP =
                new MD5CryptoServiceProvider();
            byte[] byteHash = CSP.ComputeHash(bytes);

            string hash = string.Empty;

            foreach (byte b in byteHash)
                hash += string.Format("{0:x2}", b);

            return (new Guid(hash)).ToString();
        }

    }

}