﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using TestTask.Models;
using TestTask.Services.Interfaces;

namespace TestTask.Controllers
{
    [Authorize]
    public class ProjectController : Controller
    {
        private readonly IProjectService _projectService;

        public ProjectController(IProjectService projectService)
        {
            _projectService = projectService; 
        }

        public IActionResult Back()
        {
            return RedirectToAction("Index");
        }

        public async Task<IActionResult> Index(string title, string company, string type, int page = 1)
        {
            int userId = Convert.ToInt32(HttpContext.User.Claims.ToList()[1].Value);
            int role = Convert.ToInt32(HttpContext.User.Claims.ToList()[2].Value);
            ViewData["Role"] = role;
            
            List<ProjectModel> list = null;
            int projectsCount = 0;
            int projectsPerPage = 3;
            int skip = (page - 1) * projectsPerPage;
            

            if (role == (int)UserRoles.Admin)
            {
                projectsCount = await _projectService.GetProjectsCount(0, title, company, type);
                list = await _projectService.GetAllProjects(skip, projectsPerPage, title, company, type);
            }
            else
            {
                projectsCount = await _projectService.GetProjectsCount(userId, title, company, type);
                list = await _projectService.GetProjectsByUser(userId, skip, projectsPerPage, title, company, type);
            }

            PaginationModel pagination = new PaginationModel(projectsCount, page, projectsPerPage);
            FiltersModel filters = new FiltersModel()
            {
                Title = title,
                Company = company,
                Type = type
            };
            PageModel pageModel = new PageModel()
            {
                Projects = list,
                Pagination = pagination,
                Filters = filters
            };

            return View(pageModel);
        }


        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> AddProject(CreateProjectModel model)
        {
            int userId = Convert.ToInt32(HttpContext.User.Claims.ToList()[1].Value);
            List<string> skills = new List<string>();
            skills = model.Skills.Split(',').ToList();

            await _projectService.CreateProject(model.Project, userId, skills);

            return RedirectToAction("Index");
        }

        public async Task<IActionResult> DeleteProject(int id)
        {
            var result = await _projectService.Delete(id);

            if (result == null)
                return null;

            return RedirectToAction("Index");
        }

        public async Task<IActionResult> Edit(int id)
        {
            HttpContext.Session.SetInt32("curProjectId", id);
            if (id == 0)
                id = (int)HttpContext.Session.GetInt32("curProjectId");

            var currentProject = await _projectService.Get(id);

            return View(currentProject);
        }

        [HttpPost]
        public async Task<IActionResult> EditProject(ProjectModel model)
        {
            var result = await _projectService.Edit(model);

            if (result == null)
                return null;

            return RedirectToAction("Index");
        }

        public async Task<IActionResult> Details(int id)
        {
            ProjectModel model = await _projectService.Get(id);
            if (!HttpContext.Session.Keys.Contains("curProjectId"))
                HttpContext.Session.SetInt32("curProjectId", id);

            return View(model);
        }
        
        

        public async Task<IActionResult> DeleteSkill(int id)
        {            
            var result = await _projectService.DeleteSkillProject(id);
            int projectId = (int)HttpContext.Session.GetInt32("curProjectId");
            return RedirectToAction("Edit", new { id = projectId });
        }

        public async Task<IActionResult> AddSkill(string skillName)
        {
            int projectId = (int)HttpContext.Session.GetInt32("curProjectId");


            var result = await _projectService.AddSkillProject(projectId, new SkillModel() { Skill = skillName});

            return RedirectToAction("Edit", new { id = projectId });
        }

    }
}