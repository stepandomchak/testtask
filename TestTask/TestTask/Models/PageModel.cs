﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TestTask.Models
{
    public class PageModel
    {
        public IEnumerable<ProjectModel> Projects { get; set; }
        public PaginationModel Pagination { get; set; }

        public FiltersModel Filters { get; set; }
    }
}
