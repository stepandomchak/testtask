﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TestTask.Models
{
    public class FiltersModel
    {
        public string Title { get; set; }
        public string Company { get; set; }
        public string Type { get; set; }
    }
}
