﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TestTask.Models
{
    public class PaginationModel
    {
        public int Page { get; set; }
        public int TotalPages { get; set; }

        public PaginationModel(int projectsCount, int pageNumber, int projectsPerPage)
        {
            Page = pageNumber;
            TotalPages = (int)Math.Ceiling(projectsCount / (double)projectsPerPage);
        }

        public bool HasPreviousPage
        {
            get
            {
                return (Page > 1);
            }
        }

        public bool HasNextPage
        {
            get
            {
                return (Page < TotalPages);
            }
        }
    }

}
