﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TestTask.Models
{
    public class CreateProjectModel
    {
        public ProjectModel Project { get; set; }
        
        public string Skills { get; set; }
    }
}
