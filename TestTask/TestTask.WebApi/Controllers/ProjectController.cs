﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using TestTask.Models;
using TestTask.Services.Interfaces;
using TestTask.WebApi.Models;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace TestTask.WebApi.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class ProjectController : Controller
    {
        private readonly IProjectService _projectService;

        public ProjectController(IProjectService projectService)
        {
            _projectService = projectService;
        }

        [HttpGet]
        [Route("/project/getAll/")]
        public async Task<ActionResult<IEnumerable<ProjectModel>>> GetAll(int skip, int take, string title, string company, string type)
        {
            var result = await _projectService.GetAllProjects(skip, take, title, company, type);
            return Ok(result);
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<ProjectModel>> Get(int id)
        {
            var result = await _projectService.Get(id);
            return Ok(result);
        }

        // Type : Work = 1, Book = 2, Course = 3, Blog = 4, Other = 5
        [HttpPost]
        public async Task<ActionResult> Post([FromBody]CreateProjectModel model)
        {
            var res = await _projectService.CreateProject(model.Project, model.UserId, model.Skills);

            if (res == null)
                BadRequest();

            return Ok(res);
        }

        [HttpPut()]
        public async Task<ActionResult> Put([FromBody]ProjectModel model)
        {
            var res = await _projectService.Edit(model);

            if (res == null)
                BadRequest();

            return Ok(res);
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(int id)
        {
            var res = await _projectService.Delete(id);

            if (res == null)
                BadRequest();

            return Ok(res);
        }
    }
}
