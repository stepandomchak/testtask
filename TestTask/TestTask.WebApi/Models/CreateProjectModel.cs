﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TestTask.Models;

namespace TestTask.WebApi.Models
{
    public class CreateProjectModel
    {
        public int UserId { get; set; }

        public ProjectModel Project { get; set; }

        public List<string> Skills { get; set; }
    }
}
