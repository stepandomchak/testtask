﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace TestTask.Models
{
    public class ProjectModel : BaseModel
    {
        [Required]
        public string Title { get; set; }

        [Required]
        public string Description { get; set; }

        public string Organization { get; set; }

        public DateTime Start { get; set; }
        
        public DateTime End { get; set; }

        [Required]
        public string Roles { get; set; }

        public string Link { get; set; }

        public virtual ICollection<SkillProjectModel> SkillProject { get; set; }
        
        [Required]
        public ProjectTypeModel Type { get; set; }
        
        [Required]
        public DateTime Created { get; set; }
        
        [Required]
        public DateTime Updated { get; set; }

        [Required]
        public virtual UserModel User { get; set; }

        public ProjectModel()
        {
            SkillProject = new List<SkillProjectModel>();
        }

    }
}
