﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace TestTask.Models
{
    public enum UserRoles
    {
        User = 1,
        Admin = 2
    }

    public class UserModel : BaseModel
    {
        [Required]
        public string Login { get; set; }
        [Required]
        public string Password { get; set; }
        [Required]
        public UserRoles Role { get; set; }

        public virtual ICollection<ProjectModel> Projects { get; set; }

    }
}
