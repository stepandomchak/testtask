﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace TestTask.Models
{
    public class SkillProjectModel : BaseModel
    {
        public int SkillId { get; set; }

        public virtual SkillModel Skill { get; set; }

        public int ProjectId { get; set; }

        public virtual ProjectModel Project { get; set; }
    }

}
