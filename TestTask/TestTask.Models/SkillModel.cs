﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace TestTask.Models
{
    public class SkillModel : BaseModel
    {
        [Required]
        public string Skill { get; set; }

        public virtual ICollection<SkillProjectModel> SkillProject { get; set; }

        public SkillModel()
        {
            SkillProject = new List<SkillProjectModel>();
        }

    }
}
