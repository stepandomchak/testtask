﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TestTask.Models
{
    public class ProjectTypeModel : BaseModel
    {
        public string Name { get; set; }

        public virtual List<ProjectModel> Projects { get; set; }

    }
}
