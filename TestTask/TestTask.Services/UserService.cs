﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using TestTask.Models;
using TestTask.Repositories;
using TestTask.Services.Interfaces;
using TestTask.Storage;

namespace TestTask.Services
{
    public class UserService : IUserService
    {
        ApplicationContext _context;

        public UserService(ApplicationContext context)
        {
            _context = context;
        }
        
        public async Task<UserModel> CreateUser(UserModel model)
        {
            var user = await (new UserRepository(_context)).Create(model);
            return user;
        }

        public async Task<UserModel> Edit(UserModel model)
        {
            var user = await (new UserRepository(_context)).Get(model.Id);
            if (user == null)
                return null;

            user.Login = model.Login;
            user.Password = model.Password;
            user.Role = model.Role;

            return await (new UserRepository(_context)).Edit(user);
        }

        public async Task<UserModel> Delete(int id)
        {
            var user = await (new UserRepository(_context)).Get(id);
            return await (new UserRepository(_context)).Delete(user);
        }

        public async Task<UserModel> Get(int id)
        {
            return await (new UserRepository(_context)).GetUserById(id);
        }

        public async Task<UserModel> GetUserByEmailAndPassword(string login, string password)
        {
            return await (new UserRepository(_context)).GetUserByLoginAndPassword(login, password);            
        }


    }
}
