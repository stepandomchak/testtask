﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TestTask.Models;
using TestTask.Repositories;
using TestTask.Services.Interfaces;
using TestTask.Storage;

namespace TestTask.Services
{
    public class SkillProjectService
    {
        ApplicationContext _context;

        public SkillProjectService(ApplicationContext context)
        {
            _context = context;
        }

        public async Task<SkillProjectModel> DeleteSkillProject(int projectId, int skillId)
        {
            SkillProjectModel sp = new SkillProjectModel() { Id = 5, SkillId = 4, ProjectId = 1};
            return await new SkillProjectRepository(_context).Delete(sp);
        }

        public async Task<SkillProjectModel> AddSkillProject(int projectId, int skillId)
        {
            SkillProjectModel sp = new SkillProjectModel() { Id = 5, SkillId = 4, ProjectId = 1 };
            return await new SkillProjectRepository(_context).Delete(sp);
        }

    }
}
