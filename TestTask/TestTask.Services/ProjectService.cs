﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TestTask.Models;
using TestTask.Repositories;
using TestTask.Services.Interfaces;
using TestTask.Storage;

namespace TestTask.Services
{
    public class ProjectService : IProjectService
    {
        ApplicationContext _context;

        public ProjectService(ApplicationContext context)
        {
            _context = context;
        }

        public async Task<ProjectModel> CreateProject(ProjectModel model, int userId, List<string> skills)
        {
            var user = await (new UserRepository(_context)).GetUserById(userId);
            if (user == null)
                return null;

            var type = await (new ProjectTypeRepository(_context)).GetTypeByName(model.Type.Name);
            if (type == null)            
                type = await (new ProjectTypeRepository(_context)).Create(new ProjectTypeModel() { Name = model.Type.Name });

            model.Type = type;
            model.User = user;
            model.Created = DateTime.Now;
            model.Updated = DateTime.Now;

            var project = await (new ProjectRepository(_context)).Create(model);        

            foreach (string item in skills)
            {
                var skill = await (new SkillRepository(_context).GetSkillByName(item));
                if (skill == null)
                    skill = await (new Repository<SkillModel>(_context).Create(new SkillModel() { Skill = item }));

                var skillProject = await (new Repository<SkillProjectModel>(_context)
                    .Create(new SkillProjectModel() { Project = project, Skill = skill }));
            }

            return project;
        }

        public async Task<ProjectModel> Edit(ProjectModel model)
        {
            var project = await (new ProjectRepository(_context)).GetProjectById(model.Id);
            if (project == null)
                return null;

            var type = await (new ProjectTypeRepository(_context)).GetTypeByName(model.Type.Name);
            if (type == null)
                type = await (new ProjectTypeRepository(_context)).Create(new ProjectTypeModel() { Name = model.Type.Name });


            project.Title = model.Title;
            project.Description = model.Description;
            project.Organization = model.Organization;
            project.Start = model.Start;
            project.End = model.End;
            project.Roles = model.Roles;
            project.Link = model.Link;
            project.Type = type;
            project.Updated = DateTime.Now;

            return await (new ProjectRepository(_context)).Edit(project);
        }

        public async Task<ProjectModel> Delete(int id)
        {
            ProjectModel project = await (new ProjectRepository(_context)).Get(id);
            return await (new ProjectRepository(_context)).Delete(project);
        }

        public async Task<ProjectModel> Get(int id)
        {
            var res = await (new ProjectRepository(_context)).GetProjectById(id);
            return res;
        }

        public async Task<List<ProjectModel>> GetAllProjects(int skip, int take, string title, string company, string type)
        {
            var res = await (new ProjectRepository(_context)).GetAllProjects(skip, take, title, company, type);
            return res;
        }

        public async Task<List<ProjectModel>> GetProjectsByUser(int userId, int skip, int take, string title, string company, string type)
        {
            return await(new ProjectRepository(_context)).GetProjectsByUser(userId, skip, take, title, company, type);
        }

        public async Task<int> GetProjectsCount(int userId, string title, string company, string type)
        {
            return await(new ProjectRepository(_context)).GetProjectsCount(userId, title, company, type);
        }

     
        public async Task<SkillProjectModel> DeleteSkillProject(int id)
        {
            var res = await (new SkillProjectRepository(_context)).Get(id);
            if (res == null)
                return null;

            return await new SkillProjectRepository(_context).Delete(res);
        }

        public async Task<ProjectModel> AddSkillProject(int projectId, SkillModel model)
        {
            SkillModel skill = await (new SkillRepository(_context).GetSkillByName(model.Skill));
            if(skill == null)
                skill = await (new Repository<SkillModel>(_context).Create(model));

            var project = await (new ProjectRepository(_context).Get(projectId));

            var skillProject = await (new Repository<SkillProjectModel>(_context)
                .Create(new SkillProjectModel() { Project = project, Skill = skill }));
            project.Updated = DateTime.Now;

            return await (new ProjectRepository(_context)).Edit(project);
        }

    }
}
