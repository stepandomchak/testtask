﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TestTask.Models;
using TestTask.Repositories;
using TestTask.Services.Interfaces;
using TestTask.Storage;

namespace TestTask.Services
{
    public class ProjectTypeService : ITypeService
    {
        ApplicationContext _context;

        public ProjectTypeService(ApplicationContext context)
        {
            _context = context;
        }

        public Task<ProjectTypeModel> Delete(int id)
        {
            throw new NotImplementedException();
        }

        public Task<ProjectTypeModel> Edit(ProjectTypeModel model)
        {
            throw new NotImplementedException();
        }

        public async Task<ProjectTypeModel> Get(int id)
        {
            var res = await(new ProjectTypeRepository(_context)).Get(id);
            return res;
        }

        public async Task<List<ProjectTypeModel>> GetProjectTypes()
        {
            var res = await (new ProjectTypeRepository(_context)).GetProjectTypes();
            return res;
        }

        public async Task<ProjectTypeModel> GetTypeByName(string typeName)
        {
            var res = await(new ProjectTypeRepository(_context)).GetTypeByName(typeName);
            return res;
        }
    }
}
