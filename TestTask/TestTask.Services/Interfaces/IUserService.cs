﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TestTask.Models;

namespace TestTask.Services.Interfaces
{
    public interface IUserService : IService<UserModel>
    {

        Task<UserModel> CreateUser(UserModel model);
        
        Task<UserModel> GetUserByEmailAndPassword(string login, string password);

    }
}
