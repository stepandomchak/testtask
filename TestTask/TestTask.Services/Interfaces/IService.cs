﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace TestTask.Services.Interfaces
{
    public interface IService<T>
    {        
        Task<T> Edit(T model);

        Task<T> Delete(int id);
        
        Task<T> Get(int id);

    }
}
