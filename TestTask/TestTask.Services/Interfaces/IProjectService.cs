﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TestTask.Models;

namespace TestTask.Services.Interfaces
{
    public interface IProjectService : IService<ProjectModel>
    {
        Task<List<ProjectModel>> GetProjectsByUser(int userId, int skip, int take, string title, string company, string type);      
        
        Task<ProjectModel> CreateProject(ProjectModel model, int userId, List<string> skills);

        Task<List<ProjectModel>> GetAllProjects(int skip, int take, string title, string company, string type);
        
        Task<int> GetProjectsCount(int userId, string title, string company, string type);


        Task<SkillProjectModel> DeleteSkillProject(int id);

        Task<ProjectModel> AddSkillProject(int projectId, SkillModel model);


    }
}
