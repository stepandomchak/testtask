﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TestTask.Models;

namespace TestTask.Services.Interfaces
{
    public interface ITypeService : IService<ProjectTypeModel>
    {
        Task<List<ProjectTypeModel>> GetProjectTypes();
     
        Task<ProjectTypeModel> GetTypeByName(string typeName);
    }
}
